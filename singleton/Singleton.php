<?php
/**
 * 单例模式
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 10:12
 */

namespace singleton;

class Singleton
{
    /**
     * 自身类
     * @var object $this
     * */
    private static $_instance;

    /**
     * 测试属性
     * */
    public $num = 0;

    /**
     * 构造函数
     * @return void
     * */
    private function __construct()
    {

    }

    /**
     * 魔术方法
     * 禁止 clone
     *
     * */
    public function __clone()
    {
        echo '禁止克隆'; exit();
    }

    /**
     * 获取实例
     * @return object
     * */
    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 测试
     * */
    public function test()
    {
        echo "测试单例 \n";
    }

    /**
     * 设置测试值
     * @param int
     * */
    public function setNum($num)
    {
        $this->num = $num;
    }

}