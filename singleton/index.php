<?php
/**
 * 启动单例
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 10:14
 */

// 类自动加载

spl_autoload_register('autoload');

function autoload ($class)
{
    require dirname($_SERVER['SCRIPT_FILENAME']) . '/../' . str_replace('\\', '/', $class) . '.php';
}


use singleton\Singleton;

// 获取单例
$instance = Singleton::getInstance();

$instance->test();

echo $instance->num . "\n";

$instance->setNum(11);

echo $instance->num . "\n";

// 重新获取 验证是否只有一个类
$instance = Singleton::getInstance();

echo $instance->num . "\n";

// 克隆
$instanceClone = clone $instance;