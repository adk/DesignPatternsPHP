<?php
/**
 * 种植工厂
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 15:07
 */

namespace factoryAbstract;


class PlantFactory implements Factory
{
    public function createFarm()
    {
        return new RiceFarm();
    }

    public function createZoo()
    {
        return new PeonyZoo();
    }
}