<?php
/**
 * 动物类
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 10:48
 */
namespace factoryAbstract;

class AnimalFactory implements Factory
{
    public function createFarm()
    {
        return new PigFarm();
    }

    public function createZoo()
    {
        return new PandaZoo();
    }
}