<?php
/**
 * 工厂接口
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 10:46
 */
namespace factoryAbstract;


Interface Factory
{
    // 创建农场
    public function createFarm();

    // 创建动物园
    public function createZoo();
}