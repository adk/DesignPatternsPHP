<?php
/**
 * 创建型模式
 *
 * php抽象工厂模式
 *
 * 说说我理解的工厂模式和抽象工厂模式的区别：
 * 工厂就是一个独立公司，负责生产对象；
 * 抽象工厂就是集团，负责生产子公司（工厂）；
 * @example 运行 php index.php
 */

// 类自动加载
spl_autoload_register('autoload');

function autoload($class)
{
    require dirname($_SERVER['SCRIPT_FILENAME']) . '/../' . str_replace('\\' , '/', $class) . '.php';
}

use factoryAbstract\AnimalFactory;
use factoryAbstract\PlantFactory;
use factoryAbstract\Factory;
use factoryAbstract\Income;

// 初始化一个动物生产线，包含了一票产品
$animal = new AnimalFactory();

// 初始化一个植物生产线，包含了一票产品
$plant = new PlantFactory();

// 模拟调用
function call(Factory $factory){
    // 闭包函数
    $earn = function (Income $income) {
        $income->money();
    };

    // 闭包函数调用类方法
    $earn($factory->createFarm());
    $earn($factory->createZoo());
}

call($animal);

call($plant);