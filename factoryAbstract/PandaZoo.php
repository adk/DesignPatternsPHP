<?php
/**
 * 熊猫类
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 15:24
 */

namespace factoryAbstract;


class PandaZoo implements ZooInterface
{
    // 开馆
    public function show()
    {
        echo "熊猫员开馆 \n";
    }

    public function money()
    {
        $this->show();
        echo "卖门票 \n \n";
    }
}