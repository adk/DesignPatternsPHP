<?php
/**
 * 猪类
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 15:11
 */

namespace factoryAbstract;


class PigFarm implements FarmInterface
{
    // 收成
    public function harvest()
    {
        echo "养殖部门收获猪肉（不清真）~ \n";
    }

    public function money()
    {
        $this->harvest();
        echo "卖猪肉 \n \n";
    }
}