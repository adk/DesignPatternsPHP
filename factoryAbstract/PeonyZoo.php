<?php
/**
 * 牡丹园类
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 15:24
 */

namespace factoryAbstract;


class PeonyZoo implements ZooInterface
{
    // 开馆
    public function show()
    {
        echo "牡丹员开馆 \n";
    }

    public function money()
    {
        $this->show();
        echo "卖门票 \n \n";
    }
}