<?php
/**
 * 动物园接口
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 11:08
 */

namespace factoryAbstract;


interface ZooInterface extends Income
{
    // 展览
    public function show();
}