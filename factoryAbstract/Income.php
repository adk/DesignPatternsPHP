<?php
/**
 * 收入接口
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 10:53
 */

namespace factoryAbstract;


Interface Income
{
    // 金钱
    public function money();
}