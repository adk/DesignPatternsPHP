<?php
/**
 * 农场接口
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 11:02
 */

namespace factoryAbstract;


Interface FarmInterface extends Income
{
    // 收成
    public function harvest();
}