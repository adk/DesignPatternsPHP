<?php
/**
 * 大米类
 * @describe 详细的功能描述
 * @date: 2018/1/17
 * @time: 15:11
 */

namespace factoryAbstract;


class RiceFarm implements FarmInterface
{
    // 收成
    public function harvest()
    {
        echo "种植部门收获大米~ \n";
    }

    public function money()
    {
        $this->harvest();
        echo "卖大米 \n \n";
    }
}