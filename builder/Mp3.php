<?php
/**
 * Mp3 类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 14:04
 */

namespace builder;


class Mp3 implements ProductInterface
{
    /**
     * 名称
     * @var string
     */
    private $_name = '';
    /**
     * 处理器
     * @var string
     */
    private $_cpu = '';
    /**
     * 内存
     * @var string
     */
    private $_ram = '';
    /**
     * 储存
     * @var string
     */
    private $_storage = '';
    /**
     * 系统
     * @var string
     */
    private $_os = '';

    /**
     * 构造函数
     * @param string $name
     * @param $hardware array 硬件配置
     * @param $software array 软件配置
     * */
    public function __construct($name, array $hardware = [], array $software = [])
    {
        $this->_name = $name;

        echo $this->_name . "配置如下： \n";

        // 构建硬件
        $this->hardware($hardware);

        // 构建软件
        $this->software($software);

    }

    /**
     * 构建硬件
     * @param $hardware array 硬件参数
     * @return void
     * */
    public function hardware(array $hardware = [])
    {
        // 创建 CPU
        $this->_cpu = new HardwareCpu($hardware['cpu']);

        // 创建内存
        $this->_ram = new HardwareRam($hardware['ram']);

        // 创建存储
        $this->_storage = new HardwareStorage($hardware['storage']);
    }

    /**
     * 构建软件
     * @param $software array 软件参数
     * @return void
     * */
    public function software(array $software = [])
    {
        // 创建操作系统
        $softwareOs = new SoftwareOs();
        $this->_os = $softwareOs->produce($software['os']);
    }
}