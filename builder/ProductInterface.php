<?php
/**
 * 构建器接口
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 10:53
 */

namespace builder;


interface ProductInterface
{
    /**
     * 构建硬件
     * @return void
     * */
    public function hardware();

    /**
     * 构建软件
     * @return void
     * */
    public function software();
}