<?php
/**
 * 内存类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 11:49
 */

namespace builder;


class HardwareRam implements Hardware
{
    public function __construct($size = 6)
    {
        echo '内存大小：' . $size . "G \n";
    }
}