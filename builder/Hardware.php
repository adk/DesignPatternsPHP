<?php
/**
 * 软件构造 接口
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 10:58
 */

namespace builder;


interface Hardware
{
    public function __construct();
}