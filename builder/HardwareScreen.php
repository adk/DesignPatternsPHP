<?php
/**
 * 屏幕类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 11:51
 */

namespace builder;


class HardwareScreen implements Hardware
{
    public function __construct($size = '5.0')
    {
        echo '屏幕大小：' . $size . "寸 \n";
    }
}