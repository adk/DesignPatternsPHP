<?php
/**
 * 产品类
 * @describe 详细的功能描述
 * @date: 2018/2/8
 * @time: 15:58
 * @version  v1.0
 */

namespace builder;

class Product
{
    /**
     * 名称
     * @var string
     * */
    private $name = '';

    /**
     * 硬件
     * @var array
     * */
    private $hardwares = array();

    /**
     * 软件
     * @var array
     * */
    private $softwares = array();

    /**
     * 构造函数
     * @param string $name 名称
     * */
    public function __construct($name = '')
    {
        $this->name = $name;
        echo $this->name . "配置如下： \n";
    }

    /**
     * 构建硬件
     * @param Hardware $instance 硬件参数
     * */
    public function addHardware(Hardware $instance)
    {
        $this->hardwares[] = $instance;
    }

    /**
     * 构建软件
     * @param Software $instance 软件参数
     * */
    public function addSoftware(Software $instance)
    {
        $this->softwares[] = $instance;
    }
}
