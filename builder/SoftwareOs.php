<?php
/**
 * 操作系统类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 11:29
 */

namespace builder;


class SoftwareOs implements Software
{
    /**
     * 创建操作系统
     * @param $os string 操作系统
     * */
    public function __construct($os = 'android')
    {
        echo '操作系统：' . $os . "\n";
    }
}