<?php
/**
 * CPU 类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 11:47
 */

namespace builder;


class HardwareCpu implements Hardware
{
    public function __construct($quantity = 8)
    {
        echo 'CPU核心数：' . $quantity . "核 \n";
    }
}