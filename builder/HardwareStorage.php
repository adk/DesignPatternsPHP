<?php
/**
 * 存储类
 * @describe 详细的功能描述
 * @date: 2018/1/18
 * @time: 11:53
 */

namespace builder;


class HardwareStorage implements Hardware
{
    public function __construct($size = 32)
    {
        echo '存储大小：' . $size . "G \n";
    }
}