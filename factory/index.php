<?php
/**
 * 创建型模式
 * 工厂方法模式和抽象工厂模式的核心区别
 * 工厂方法模式利用继承，抽象工厂模式利用组合
 * 工厂方法模式产生一个对象，抽象工厂模式产生一族对象
 * 工厂方法模式利用子类创造对象，抽象工厂模式利用接口的实现创造对象
 * 工厂方法模式可以退化为简单工厂模式(非23中GOF)
 *
 * php工厂模式
 * @example 运行 php index.php
 */

spl_autoload_register('autoload');

function autoload ($class)
{
    require dirname($_SERVER['SCRIPT_FILENAME']) . '/../' . str_replace('\\', '/', $class) . '.php';
}

// 工厂模式
use factory\Zoo;
use factory\Farm;
use factory\SampleFactory;

// 初始化农场工厂
$farm = new Farm();

// 生产一只鸡
echo "鸡的质量：". $farm->produce('chicken')->getQuality() . "\n";

// 生产一只猪
$farm->produce('pig');

// 初始化一个动物园
$zoo = new Zoo();

// 生产一只鸡
$zoo->produce('chicken');

// 生产一只猪
$zoo->produce('pig');

// 简单工厂
echo "鸡的质量" . SampleFactory::produce('chicken')->getQuality() . "\n";
echo "猪的质量" . SampleFactory::produce('pig')->getQuality() . "\n";
