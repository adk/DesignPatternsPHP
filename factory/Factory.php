<?php
/**
 * 生产工厂接口
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 14:25
 */

namespace factory;


Interface Factory
{
    // 生产方法
    public function produce();
}