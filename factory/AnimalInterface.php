<?php
/**
 * 抽象工厂
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 11:45
 */

namespace factory;

// 动物接口
Interface AnimalInterface
{
    // 获取质量
    public function getQuality();
}