<?php
/**
 * 鸡 类
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 11:43
 */

namespace factory;


class Chicken implements AnimalInterface
{
    // 质量
    private $quality;

    public function __construct($quality)
    {
        echo "生产了一只鸡~ \n";
        $this->quality = $quality;
    }

    public function getQuality()
    {
        return $this->quality;
    }
}