<?php
/**
 * 农场
 * @describe 生产动物
 * @date: 2018/1/16
 * @time: 14:29
 */

namespace factory;


class Farm implements Factory
{
    public function __construct()
    {
        echo "初始化了一个农场 \n";
    }

    /**
     * 生产方法
     * 生产动物
     * @param $type string 类型
     * @return object|string
     * */
    public function produce($type = '')
    {
        switch ($type) {
            case 'chicken':
                return new Chicken(11);
                break;
            case 'pig':
                return new Pig(21);
                break;
            default:
                echo "该农场不支持生产该农务~ \n";
                break;
        }
    }
}