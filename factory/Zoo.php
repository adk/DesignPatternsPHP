<?php
/**
 * 动物园
 * @describe 生产动物
 * @date: 2018/1/16
 * @time: 14:39
 */

namespace factory;


class Zoo implements Factory
{
    public function __construct()
    {
        echo "初始化了一个动物园~ \n";
    }

    /**
     * 生产方法
     * 生产动物
     * @param $type string
     * @return object|string
     * */
    public function produce($type = '')
    {
        switch ($type) {
            case 'chicken':
                return new Chicken(12);
                break;
            case 'pig':
                return new Pig(22);
                break;
            default:
                echo "该动物园不支持生产该动物~ \n";
        }
    }
}
