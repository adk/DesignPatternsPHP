<?php
/**
 * 猪 类
 * @describe 详细的功能描述
 * @date: 2018/1/16
 * @time: 11:48
 */

namespace factory;


class Pig implements AnimalInterface
{
    // 质量
    private $quality;

    public function __construct($quality)
    {
        echo "生产了一只猪~ \n";
        $this->quality = $quality;
    }

    public function getQuality()
    {
        return $this->quality;
    }
}
