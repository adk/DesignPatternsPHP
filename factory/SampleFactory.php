<?php
/**
 * 简单工厂
 * @describe 生产动物，无需实现动物工厂接口
 * @date: 2018/1/16
 * @time: 11:34
 */

namespace factory;

class SampleFactory
{
    /**
     * 生产方法
     *
     * 生产动物
     * @param $type string 动物类型
     * @return mixed
     * */
    public static function produce($type = '')
    {
        switch ($type) {
            case 'chicken':
                return new Chicken(10);
                break;
            case 'pig':
                return new Pig(20);
                break;
            default:
                echo "该工厂不支持生产此动物~ \n";
        }
    }
}

